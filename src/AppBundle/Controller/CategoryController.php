<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CategoryController extends Controller
{
    public function indexAction ()
    {
        return $this->render('category/index.html.twig', [
            'categories' => $this->getDoctrine()->getRepository('AppBundle:Category')->findAll(),
        ]);
    }

    public function createAction (Request $request)
    {
        $category = new Category();

        $form = $this->createForm(\AppBundle\Form\Category::class, $category);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('category_show', ['id' => $category->getId()]);
        }

        return $this->render('category/create.html.twig', [
            'form' => $form->createView(),
            'category' => $category,
        ]);
    }

    public function showAction ($id)
    {
        /** @var Category $category */
        $category = $this->getDoctrine()
            ->getRepository('AppBundle:Category')
            ->find($id);

        if ($category === null) {
            throw new NotFoundHttpException('No category found');
        }

        return $this->render('category/show.html.twig', [
            'category' => $category,
        ]);
    }

    public function updateAction (Request $request, $id)
    {
        $em   = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AppBundle:Category')->find($id);

        if ($category === null) {
            throw new NotFoundHttpException('No category found');
        }

        $form = $this->createForm(\AppBundle\Form\Category::class, $category);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('category_show', ['id' => $category->getId()]);
        }

        return $this->render('category/update.html.twig', [
            'form' => $form->createView(),
            'category' => $category,
        ]);
    }

    public function deleteAction ($id)
    {
        $em   = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AppBundle:Category')->find($id);

        if ($category === null) {
            throw new NotFoundHttpException('No category found');
        }

        $em->remove($category);
        $em->flush();

        return $this->redirectToRoute('categories');
    }

}
