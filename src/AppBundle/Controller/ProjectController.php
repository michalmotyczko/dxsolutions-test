<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Project;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProjectController extends Controller
{
    public function indexAction ()
    {
        return $this->render('project/index.html.twig', [
            'projects' => $this->getDoctrine()->getRepository('AppBundle:Project')->findAll(),
        ]);
    }

    public function createAction (Request $request)
    {
        $project = new Project();

        $form = $this->createForm(\AppBundle\Form\Project::class, $project);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($project);
            $em->flush();

            return $this->redirectToRoute('project_show', ['id' => $project->getId()]);
        }

        return $this->render('project/create.html.twig', [
            'form' => $form->createView(),
            'project' => $project,
        ]);
    }

    public function showAction ($id)
    {
        /** @var Project $project */
        $project = $this->getDoctrine()
            ->getRepository('AppBundle:Project')
            ->find($id);

        if ($project === null) {
            throw new NotFoundHttpException('No project found');
        }

        return $this->render('project/show.html.twig', [
            'project' => $project,
        ]);
    }

    public function updateAction (Request $request, $id)
    {
        $em   = $this->getDoctrine()->getManager();
        $project = $em->getRepository('AppBundle:Project')->find($id);

        if ($project === null) {
            throw new NotFoundHttpException('No project found');
        }

        $form = $this->createForm(\AppBundle\Form\Project::class, $project);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($project);
            $em->flush();

            return $this->redirectToRoute('project_show', ['id' => $project->getId()]);
        }

        return $this->render('project/update.html.twig', [
            'form' => $form->createView(),
            'project' => $project,
        ]);
    }

    public function deleteAction ($id)
    {
        $em   = $this->getDoctrine()->getManager();
        $project = $em->getRepository('AppBundle:Project')->find($id);

        if ($project === null) {
            throw new NotFoundHttpException('No project found');
        }

        $em->remove($project);
        $em->flush();

        return $this->redirectToRoute('projects');
    }
}
