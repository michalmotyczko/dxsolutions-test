<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Task;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TaskController extends FOSRestController
{
    public function getTasksAction ()
    {
        $tasks = $this->getDoctrine()->getRepository('AppBundle:Task')->findAll();
        $view = $this->view(['tasks' => $tasks], 200)
            ->setTemplate("task/index.html.twig")
            ->setTemplateVar('users');

        return $this->handleView($view);
    }

    public function newTaskAction (Request $request)
    {
        $task = new Task();

        $form = $this->createForm(\AppBundle\Form\Task::class, $task, [
            'action' => $this->generateUrl('post_tasks'),
        ]);

        $view = new View($form);

        $view->setTemplate('task/create.html.twig');
        return $this->get('fos_rest.view_handler')->handle($view);
    }

    public function postTasksAction(Request $request)
    {
        $task = new Task();
        $form = $this->createForm(\AppBundle\Form\Task::class, $task);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();
            $view = View::createRouteRedirect('get_task', array('id' => $form->getData()->getId()));
        } else {
            $view = View::create($form);
            $view->setTemplate('task/create.html.twig');
        }

        return $this->get('fos_rest.view_handler')->handle($view);
    }

    public function putTaskAction(Request $request, $id)
    {
        /** @var Task $task */
        $task = $this->getDoctrine()
            ->getRepository('AppBundle:Task')
            ->find($id);

        $form = $this->createForm(\AppBundle\Form\Task::class, $task);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();
            $view = View::createRouteRedirect('get_task', array('id' => $form->getData()->getId()));
        } else {
            $view = View::create($form);
            $view->setTemplate('task/update.html.twig');
        }

        return $this->get('fos_rest.view_handler')->handle($view);
    }

    public function getTaskAction($id)
    {
        /** @var Task $task */
        $task = $this->getDoctrine()
            ->getRepository('AppBundle:Task')
            ->find($id);

        if ($task === null) {
            throw new NotFoundHttpException('No task found');
        }

        $view = new View($task);
        $view->setTemplate('task/show.html.twig');
        return $this->get('fos_rest.view_handler')->handle($view);
    }

    public function editTaskAction (Request $request, $id)
    {
        $em   = $this->getDoctrine()->getManager();
        $task = $em->getRepository('AppBundle:Task')->find($id);
        if ($task === null) {
            throw new NotFoundHttpException('No task found');
        }

        $form = $this->createForm(\AppBundle\Form\Task::class, $task, [
            'action' => $this->generateUrl('post_tasks'),
        ]);
        $view = new View($form);
        $view->setTemplate('task/update.html.twig');
        return $this->get('fos_rest.view_handler')->handle($view);
    }

    public function deleteTaskAction ($id)
    {
        $em   = $this->getDoctrine()->getManager();
        $task = $em->getRepository('AppBundle:Task')->find($id);

        if ($task === null) {
            throw new NotFoundHttpException('No task found');
        }

        $em->remove($task);
        $em->flush();

        return $this->redirectToRoute('get_tasks');
    }

    public function markAsCompletedAction ($id)
    {
        $em   = $this->getDoctrine()->getManager();
        $task = $em->getRepository('AppBundle:Task')->find($id);

        if ($task === null) {
            throw new NotFoundHttpException('No task found');
        }

        $task->setCompleted(true);
        $em->persist($task);
        $em->flush();

        return $this->redirectToRoute('get_task', ['id' => $task->getId()]);
    }
}
