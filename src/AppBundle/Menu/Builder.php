<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 13.07.16
 * Time: 22:35
 */

namespace AppBundle\Menu;

use AppBundle\Entity\Category;
use AppBundle\Entity\Project;
use AppBundle\Entity\Task;
use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function rightNavbar(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $menu->addChild('Logout', ['route' => 'fos_user_security_logout']);
        } else {
            $menu->addChild('Login', ['route' => 'fos_user_security_login']);
        }

        return $menu;
    }

    public function leftNavbar(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        if (!$this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $menu;
        }

        $menu->addChild('Tasks', ['route' => 'tasks']);
        $menu->addChild('Categories', ['route' => 'categories']);
        $menu->addChild('Projects', ['route' => 'projects']);

        return $menu;
    }

    public function tasks(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        /** @var Task $task */
        $task = isset($options['task']) ? $options['task'] : null;
        $menu->addChild('New Task', ['route' => 'new_task']);
        if ($task === null) {
            return $menu;
        }

        if (!$task->isCompleted()) {
            $menu->addChild('Update', ['route' => 'edit_task', 'routeParameters' => ['id' => $task->getId()]]);
        }

        $menu->addChild('Delete', ['route' => 'delete_task', 'routeParameters' => ['id' => $task->getId()]]);

//        if (!$task->isCompleted()) {
//            $menu->addChild('Done', ['route' => 'task_complete', 'routeParameters' => ['id' => $task->getId()]]);
//        }

        return $menu;
    }

    public function categories(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        /** @var Category $category */
        $category = isset($options['category']) ? $options['category'] : null;
        $menu->addChild('New Category', ['route' => 'category_create']);
        if ($category === null) {
            return $menu;
        }

        $menu->addChild('Update', ['route' => 'category_update', 'routeParameters' => ['id' => $category->getId()]]);
        $menu->addChild('Delete', ['route' => 'category_delete', 'routeParameters' => ['id' => $category->getId()]]);

        return $menu;
    }

    public function projects(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        /** @var Project $project */
        $project = isset($options['project']) ? $options['project'] : null;
        $menu->addChild('New Project', ['route' => 'project_create']);
        if ($project === null) {
            return $menu;
        }

        $menu->addChild('Update', ['route' => 'project_update', 'routeParameters' => ['id' => $project->getId()]]);
        $menu->addChild('Delete', ['route' => 'project_delete', 'routeParameters' => ['id' => $project->getId()]]);

        return $menu;
    }
}