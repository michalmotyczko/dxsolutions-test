<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 14.07.16
 * Time: 18:42
 */

namespace AppBundle\Form;

use AppBundle\Entity\Category;
use AppBundle\Entity\Project;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class Task extends AbstractType
{
    public function buildForm (FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class)
            ->add('description', TextareaType::class, ['required' => false])
            ->add('deadline', DateType::class, ['widget' => 'single_text'])
            ->add('completed', CheckboxType::class, ['required' => false])
            ->add('category', EntityType::class, ['class' => Category::class, 'choice_label' => 'name'])
            ->add('project', EntityType::class, ['class' => Project::class, 'choice_label' => 'name'])
            ->add('save', SubmitType::class, ['label' => 'Save']);
    }
}