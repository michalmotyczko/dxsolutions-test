Test
========================

Simple CRUD with OAuth2 login.

What's inside?
--------------

4 Entities:

 - User
 - Task
 - Category
 - Project

Bundles used:

- FOSUserBundle
- HWIOAuthBundle
- FOSRestBundle
- JMSSerializerBundle
- AsseticBundle
- BraincraftedBootstrapBundle

To do
-----

There are still things I'd like to do but didn't have enough time.

What's still missing:

- tests
- proper REST access for all controllers/entities,
- pagination and sorting on index pages,
- better validation (such as min allowed dates etc),
- timestamp and blame attributes on models (created_at, updated_at, created_by, updated_by) with auto update,